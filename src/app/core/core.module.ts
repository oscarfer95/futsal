import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavbarComponent, WrapperComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [WrapperComponent]
})
export class CoreModule { }
