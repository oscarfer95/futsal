import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TeamFormComponent } from './team-form/team-form.component';
import { PlayerFormComponent } from './player-form/player-form.component';
import { MatchFormComponent } from './match-form/match-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
  FormComponent,
  TeamFormComponent,
  PlayerFormComponent,
  MatchFormComponent
],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports:[
    FormComponent,
    ReactiveFormsModule,
    TeamFormComponent,
    PlayerFormComponent,
    MatchFormComponent,
    FormsModule
  ]
})
export class SharedModule { }