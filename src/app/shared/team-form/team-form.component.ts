import { League } from './../models/league.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Team } from './../models/team.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.scss']
})
export class TeamFormComponent implements OnInit {

  public team :Team;
  public isModalHidden: boolean = true;
  public pos:number;
  teamForm: FormGroup;
  league = new League("");

  constructor() {
  }

  ngOnInit() {
    this.createFormController();
    this.team = new Team("");
  }

  createFormController() {
    this.teamForm = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.maxLength(20)])
    });
  }

  showModal(league){
    this.league = league;
    this.team = new Team("");
    this.isModalHidden = false;
  }

  save(){
    if (this.league.championships[this.pos].teams.length < 8) {
      this.league.addTeam(this.pos,this.team);
    } else {
      alert("Limite de equipos alcanzado");
    }
    this.isModalHidden = true;
  }

  hideModal(){
    this.isModalHidden = true;
  }

  changeSelected(pos) {
    const elements = document.getElementsByClassName("item");
    for (let i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    this.pos = pos;
    elements[pos].classList.add("selected");
  }

}
