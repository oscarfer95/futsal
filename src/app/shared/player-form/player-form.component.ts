import { Championship } from './../models/championship.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Player } from './../models/player.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-player-form',
  templateUrl: './player-form.component.html',
  styleUrls: ['./player-form.component.scss']
})
export class PlayerFormComponent implements OnInit {

  public player =  new Player("","","");
  public isModalHidden: boolean = true;
  public pos:number;
  playerForm: FormGroup;
  championship = new Championship("");

  constructor() {
  }

  ngOnInit() {
    this.createFormController();
    this.player = new Player("","","");
  }

  createFormController() {
    this.playerForm = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.maxLength(20)]),
      'lastName': new FormControl('', [Validators.required, Validators.maxLength(20)]),
      'id': new FormControl('', [Validators.required, Validators.maxLength(20)])
    });
  }

  showModal(championship){
    this.championship = championship;
    this.player = new Player("","","");
    this.isModalHidden = false;
  }

  save(){
    if (this.championship.teams[this.pos].players.length < 9) {
      this.championship.teams[this.pos].addPlayer(this.player);
    } else {
      alert("Límite de jugadores alcanzado");
    }
    this.isModalHidden = true;
  }

  hideModal(){
    this.isModalHidden = true;
  }

  changeSelectedItem(pos) {
    const elements = document.getElementsByClassName("item-team");
    for (let i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    this.pos = pos;
    elements[this.pos].classList.add("selected");
  }

}
