import { Player } from './../models/player.model';
import { Information } from './../models/information.model';
import { Championship } from './../models/championship.model';
import { Match } from './../models/match.model';
import { Component, OnInit } from '@angular/core';
import { Team } from '../models/team.model';

@Component({
  selector: 'app-match-form',
  templateUrl: './match-form.component.html',
  styleUrls: ['./match-form.component.scss']
})
export class MatchFormComponent implements OnInit {

  public match : Match;
  public isModalHidden: boolean = true;
  public pos:number;
  championship = new Championship("");
  public information: Information[];

  constructor() {
    this.information = [];
    this.initArray();
  }

  ngOnInit() {
    this.match =  new Match(new Team(""),new Team(""));
    this.information = [];
    this.initArray();
  }

  showModal(championship){
    this.championship = championship;
    this.match =  new Match(new Team(""),new Team(""));
    this.initArray();
    this.isModalHidden = false;
  }

  initArray() {
    this.information = [];
    for (let i = 0; i < 20; i++) {
      this.information.push(new Information(new Player("","",""),0,0,0,0));
    }
  }

  save(){
    this.fillPlayers();
    this.match.information = this.information;
    if (this.championship.matches.length < 7) {
        this.championship.addMatch(this.match);
    } else {
      alert("Límite de partidos alcanzado");
    }
    this.isModalHidden = true;
  }

  fillPlayers() {
    for (let i = 0; i < this.match.team1.players.length; i++) {
      this.information[i].player = this.match.team1.players[i];
      this.match.score1 = +this.information[i].goals;
    }
    for (let j = 0; j < this.match.team2.players.length; j++) {
      this.information[10+j].player = this.match.team2.players[j];
      this.match.score2 = +this.information[10+j].goals;
    }
  }

  hideModal(){
    this.isModalHidden = true;
  }

  changeSelectedItem(pos) {
    const elements = document.getElementsByClassName("item-team");
    for (let i = 0; i < elements.length; i++) {
      elements[i].classList.remove("selected");
    }
    this.pos = pos;
    elements[this.pos].classList.add("selected");
  }

  setTeam1(event) {
    if (event.target.selectedIndex) {
      this.match.team1 = this.championship.teams[event.target.selectedIndex-1];
    } else {
      this.match.team1 = null;
    }
    
  }

  setTeam2(event) {
    if (event.target.selectedIndex) {
      this.match.team2 = this.championship.teams[event.target.selectedIndex-1];
    } else {
      this.match.team2 = null;
    }
  }


}
