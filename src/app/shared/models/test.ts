import { Information } from './information.model';
import { Player } from './player.model';
import { Championship } from './championship.model';
import { Match } from './match.model';
import { Team } from './team.model';

export class Test {
      championship =  new Championship("NacionalsitoFutsalTest");
      information: Information;
      constructor() {
            this.addTeams();
            this.addPlayersToTeams();
            this.addMatch();
            this.championship.setTestArray();
      }
      addTeams() {
            this.championship.addTeam(new Team("Wilstermann"));
            this.championship.addTeam(new Team("Aurora"));
            this.championship.addTeam(new Team("Bolivar"));
            this.championship.addTeam(new Team("Stronger"));
            this.championship.addTeam(new Team("Real Potosí"));
            this.championship.addTeam(new Team("A. Guabirá"));
            this.championship.addTeam(new Team("Blooming"));
            this.championship.addTeam(new Team("O. Petrolero"));
      }
      addMatch() {
            this.championship.addMatch(new Match(this.championship.teams[0],this.championship.teams[1]));
            this.championship.addMatch(new Match(this.championship.teams[2],this.championship.teams[3]));
            this.championship.addMatch(new Match(this.championship.teams[4],this.championship.teams[5]));
            this.championship.addMatch(new Match(this.championship.teams[6],this.championship.teams[7]));
            this.championship.addMatch(new Match(this.championship.teams[0],this.championship.teams[2]));
            this.championship.addMatch(new Match(this.championship.teams[4],this.championship.teams[6]));
            this.championship.addMatch(new Match(this.championship.teams[0],this.championship.teams[4]));
      }

      addPlayersToTeams() {
            for (let i = 0; i < this.championship.teams.length; i++) {
                  this.addPlayers(this.championship.teams[i]);
            }
      }

      addPlayers(team: Team) {
            for (let i = 0; i < 10; i++) {
                  team.addPlayer(new Player("Nombre"+i,"Apellido"+i,team.name+i));
            }
      }
}