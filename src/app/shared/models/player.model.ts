export class Player {
      name: String;
      lastName: String;
      id: String;
      constructor(name, lastName, id) {
            this.name = name;
            this.lastName = lastName;
            this.id = id;
      }
}