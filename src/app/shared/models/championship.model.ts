import { Prize } from './prize.model';
import { Match } from './match.model';
import { Team } from './team.model';
export class Championship {
      name: String;
      teams: Team[];
      matches: Match[];
      prize: Prize;
      constructor(name) {
            this.name = name;
            this.teams = [];
            this.matches = [];
            this.prize = new Prize();
      }

      addTeam(team:Team) {
            if (this.teams.length < 8) {
                  this.teams.push(team);
            } else {
                  alert("Limite de equipos alcanzado");
            }
      }

      addMatch(match: Match) {
            if (this.matches.length < 7) {
                  this.matches.push(match);
            } else {
                  alert("Limite de partidos alcanzado");
            } 
      }

      setStadistics() {
            this.prize.getBest(this.matches);
            this.prize.setPlaces(this.matches);
      }

      setTestArray() {
            for (let i = 0; i < this.matches.length; i++) {
                  this.matches[i].initArrayTest();
            }
      }
}