import { Information } from './information.model';
import { Player } from './player.model';
import { Team } from './team.model';

export class Match {
      
      team1: Team;
      team2: Team;
      score1: number;
      score2: number;
      information: Array<Information>;

      constructor(team1:Team, team2:Team){
            this.team1 = team1;
            this.team2 = team2;
            this.score1 = 0;
            this.score2 = 0;
            this.information = [];
            this.initArray();
            this.setScores();
      }

      getWinner() {
            if (this.score2 === this.score1) {
                  return null;
            } else {
                  return this.score1 > this.score2? this.team1 :  this.team2;
            }   
      }

      setScores() {
            for (let i = 0; i < this.team1.players.length; i++) {
                  this.information[i].player = this.team1.players[i];
                  this.score1 = this.score1 + this.information[i].goals;
            }
            for (let j = 0; j < this.team2.players.length; j++) {
                  this.information[10+j].player = this.team2.players[j];
                  this.score2 = this.score2 + this.information[10+j].goals;
            }
            
      }

      initArray() {
            for (let i = 0; i < this.team1.players.length; i++) {
                  this.information.push(new Information(this.team1.players[i],1,0,0,0));
            }
            for (let j = 0; j < this.team2.players.length; j++) {
                  this.information.push(new Information(this.team2.players[j],0,0,0,0));
            }
      }

      initArrayTest() {
            for (let i = 0; i < this.team1.players.length; i++) {
                  this.information.push(new Information(this.team1.players[i],1,0,0,0));
            }
            for (let j = 0; j < this.team2.players.length; j++) {
                  this.information.push(new Information(this.team2.players[j],0,0,2,1));
            }
      }

      getShooters() {
            let aux = [];
            for (let i = 0; i < this.information.length; i++) {
                  aux.push([this.information[i].player,this.information[i].goals])
            }
            return aux;
      }

      getAssists() {
            let aux = [];
            for (let i = 0; i < this.information.length; i++) {
                  aux.push([this.information[i].player,this.information[i].assists])
            }
            return aux;
      }

      getYellowCards() {
            let aux = [];
            for (let i = 0; i < this.information.length; i++) {
                  aux.push([this.information[i].player,this.information[i].yellowCard])
            }
            return aux;
      }

      getRedCards() {
            let aux = [];
            for (let i = 0; i < this.information.length; i++) {
                  aux.push([this.information[i].player,this.information[i].redCard])
            }
            return aux;
      }

}