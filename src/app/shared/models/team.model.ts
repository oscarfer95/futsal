import { Player } from './player.model';

export class Team {
      name: string;
      players: Player[]; //min-7 max-10
      constructor(name){
            this.name = name;
            this.players = [];
      }
      addPlayer(player: Player) {
            this.players.push(player);
      }
      getPlayers() {
            return this.players;
      }
}