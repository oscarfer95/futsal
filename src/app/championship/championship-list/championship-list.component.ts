import { Match } from './../../shared/models/match.model';
import { Team } from './../../shared/models/team.model';
import { Test } from './../../shared/models/test';
import { Prize } from './../../shared/models/prize.model';
import { MatchFormComponent } from './../../shared/match-form/match-form.component';
import { PlayerFormComponent } from './../../shared/player-form/player-form.component';
import { TeamFormComponent } from './../../shared/team-form/team-form.component';
import { FormComponent } from './../../shared/form/form.component';
import { League } from './../../shared/models/league.model';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-championship-list',
  templateUrl: './championship-list.component.html',
  styleUrls: ['./championship-list.component.scss']
})
export class ChampionshipListComponent implements OnInit {

  league = new League('Liga 1');
  test = new Test();
  team = new Team("");
  match : Match;
  @ViewChild('alert', {static: false}) championshipForm: FormComponent;
  @ViewChild('teamform', {static: false}) teamForm: TeamFormComponent;
  @ViewChild('playerform', {static: false}) playerForm: PlayerFormComponent;
  @ViewChild('matchform', {static: false}) matchForm: MatchFormComponent;
  public prize = new Prize();
  constructor() { 
    this.match = null;
  }

  ngOnInit() {
    this.scrollTop();
  }

  scrollTop() {
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
  }

  showHiddenButtons() {
    const items = document.getElementsByClassName("flag");
    for (let i = 0; i < items.length; i++) {
      items[i].classList.contains("hidden")? items[i].classList.remove("hidden"): items[i].classList.add("hidden");
    }
  }

  changeStadistics(pos) {
    if (this.league.championships[pos].matches.length !== 7) {
      alert("Debe ingresar todos los partidos del campeonato");
    } else {
      this.league.championships[pos].setStadistics();
      this.flashContainer();
      this.prize = this.league.championships[pos].prize;
    }
  }

  changeStadistics2() {
    this.test.championship.setStadistics();
    this.flashContainer();
    this.prize = this.test.championship.prize;
  }

  addTeam(){
    this.openTeamForm();
  }

  openTeamForm() {
    if (this.league.championships.length < 1) {
      alert("Debes registrar un campeonato");
    } else {
      this.teamForm.showModal(this.league);
    }
  }

  addPlayer(pos){
    if (this.league.championships[pos].teams.length < 1) {
      alert("Debes registrar un equipo para añadir un jugador");
    } else {
      this.playerForm.showModal(this.league.championships[pos]);
    }
  }

  addMatch(pos){
    if (this.league.championships[pos].teams.length < 8) {
      alert("Debes registrar 8 equipos en este campeonato para registrar un partido");
    } else {
      if(this.league.championships[pos].prize.aux.length === 7) {
        this.league.championships[pos].prize.setBest();
      }
      this.matchForm.showModal(this.league.championships[pos]);
    }
  }

  addChampionship() {
    this.championshipForm.showModal(this.league);
  }

  flashContainer() {
    const info = document.getElementById('flash'); 
    info.animate([
      { opacity: 0 }, 
      { opacity: 1 }
    ],
    {duration: 300}
    );
  }

  displayInfo(team: Team) {
    this.team = team;
    const el = document.getElementById("hide-cont");
    el.classList.remove("hidden");
  }

  hideInfo() {
    const el = document.getElementById("hide-cont");
    el.classList.add("hidden");
  }

  displayInfoMatch(match) {
    this.match = match;
    const el = document.getElementById("hide-cont-match");
    el.classList.remove("hidden");
  }

  hideInfoMatch() {
    const el = document.getElementById("hide-cont-match");
    el.classList.add("hidden");
  }

}
